package com.hashmiusama.bookmanager.controller;

import com.hashmiusama.bookmanager.daoimpl.EmployeeDAO;
import com.hashmiusama.bookmanager.models.EmployeeModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by uhashmi on 5/26/2016.
 */
@WebServlet(name = "EmployeeController",
        urlPatterns = "/employee")
public class EmployeeController extends HttpServlet {
    EmployeeDAO dao = new EmployeeDAO();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = "nulls";
        String name = "nulls";
        String phoneNumber = "nulls";
        EmployeeModel employee= new EmployeeModel();
        ArrayList<Object> employees = null;
        PrintWriter out = resp.getWriter();
        try{
            name = req.getParameter("name");
            if(!name.equals("nulls")){
                try{
                    employees = dao.getByName(name);
                    System.out.println(employees.toString());
                    out.print(employees.toString());
                }catch(NullPointerException e){
                    System.out.println("[]");
                    e.printStackTrace();
                }
            }
        }
        catch(NullPointerException e){}
        try{
            String idea = req.getParameter("id");
            id = idea;
            if(!id.equals("nulls")){
                try {
                    employee = (EmployeeModel) dao.getById(Integer.parseInt(id));
                    out.print(employee.toString());
                }catch(NullPointerException e){
                    e.printStackTrace();
                    out.println("{}");
                }
            }
        }
        catch(NullPointerException e){}
        try{
            phoneNumber = req.getParameter("phoneNumber");
            if(!phoneNumber.equals("nulls")){
                try{
                    employees = dao.getByPhoneNumber(phoneNumber);
                    System.out.println(employees.toString());
                    out.print(employees.toString());
                }catch(NullPointerException e){
                    System.out.println("[]");
                    e.printStackTrace();
                }
            }
        }
        catch(NullPointerException e){}
        if(req.getParameter("phoneNumber")== null && req.getParameter("id")== null && req.getParameter("name")== null){
            employees = dao.getAll();
            System.out.println(employees.toString());
            out.print(employees.toString());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            int id = Integer.parseInt(req.getParameter("id"));
            EmployeeModel employee = (EmployeeModel) dao.getById(id);
            resp.getWriter().print(dao.delete(employee));
        }catch(NullPointerException e){}
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EmployeeModel employee = new EmployeeModel();
        PrintWriter out = resp.getWriter();
        try{
            employee.setName(req.getParameter("name"));
            employee.setAddress(req.getParameter("address"));
            employee.setPhoneNumber(new BigInteger(req.getParameter("phoneNumber")));
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date myDate = null;
            java.sql.Date sqlDate = null;
            try {
                myDate = (Date) formatter.parse(req.getParameter("joiningDate"));
                sqlDate = new java.sql.Date(myDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            employee.setJoiningDate(sqlDate);
            System.out.println("inserting "+employee.toString());
            if(dao.insert(employee)){
                out.print(true);
            }else{
                out.print(false);
            }
        }catch(NullPointerException e){e.printStackTrace();}
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EmployeeModel employee = new EmployeeModel();
        PrintWriter out = resp.getWriter();
        try{
            System.out.println(Integer.parseInt(req.getParameter("id")));
            employee = (EmployeeModel) dao.getById(Integer.parseInt(req.getParameter("id")));
            System.out.println(employee.toString());
            try{
                String name = req.getParameter("name");
                if(name != null){
                    employee.setName(name);

                }
            }catch(NullPointerException e){}
            try{
                BigInteger phoneNumber = new BigInteger(req.getParameter("phoneNumber"));
                if(phoneNumber.intValue() > 0) {
                    employee.setPhoneNumber(phoneNumber);

                }
            }catch(NullPointerException e){} catch (NumberFormatException e){}
            try{
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date myDate = null;
                java.sql.Date sqlDate = null;
                try {
                    myDate = (Date) formatter.parse(req.getParameter("joiningDate"));
                    sqlDate = new java.sql.Date(myDate.getTime());
                    employee.setJoiningDate(sqlDate);
                } catch (ParseException e) {}
            }catch(NullPointerException e){}
            try{
                String address = req.getParameter("address");
                if(address != null){
                    employee.setAddress(address);
                }
            }catch(NullPointerException e){}
            System.out.println(employee.toString());
            out.print(dao.update(employee));
        }catch(NullPointerException e){e.printStackTrace();}
    }
}
