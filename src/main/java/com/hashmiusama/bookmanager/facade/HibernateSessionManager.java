package com.hashmiusama.bookmanager.facade;
import com.hashmiusama.bookmanager.models.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
public class HibernateSessionManager {
    private static SessionFactory sessionFactory;
    public static void buildSessionFactory(String classname) {
        try {
            if(classname.equals("book")){
                sessionFactory = new Configuration().
                        configure().addAnnotatedClass(BookModel.class).buildSessionFactory();
            }else if(classname.equals("customer")){
                sessionFactory = new Configuration().
                        configure().addAnnotatedClass(CustomerModel.class).buildSessionFactory();
            }else if(classname.equals("employee")){
                sessionFactory = new Configuration().
                        configure().addAnnotatedClass(EmployeeModel.class).buildSessionFactory();
            }else if(classname.equals("supervisor")){
                sessionFactory = new Configuration().
                        configure().addAnnotatedClass(SupervisorModel.class).buildSessionFactory();
            }if(classname.equals("sales")){
                sessionFactory = new Configuration().
                        configure().addAnnotatedClass(SalesModel.class).buildSessionFactory();
            }
        } catch (Throwable ex) {
            System.err.println("SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    public static void shutdown() {
// Close caches and connection pools
        getSessionFactory().close();
    }
}
