package com.hashmiusama.bookmanager.models;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by uhashmi on 5/26/2016.
 */
@Entity
@Table(name="customers")
public class CustomerModel {
    @Id
    @GeneratedValue()
    private int id;
    private String name;
    private String address;
    @Column(name="joiningdate")
    private Date joiningDate;
    @Column(name="phonenumber")
    private BigInteger phoneNumber;

    public CustomerModel(String name, String address, Date joiningDate, BigInteger phonenumber) {
        this.name = name;
        this.address = address;
        this.joiningDate = joiningDate;
        this.phoneNumber = phonenumber;
    }

    public CustomerModel(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public BigInteger getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(BigInteger phonenumber) {
        this.phoneNumber = phonenumber;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id + '\"' +
                ", \"name\":\"" + name + '\"' +
                ", \"address\":\"" + address + '\"' +
                ", \"joiningDate\":\"" + joiningDate + '\"' +
                ", \"phoneNumber\":\"" + phoneNumber +'\"'+
                '}';
    }
}
