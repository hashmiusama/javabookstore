package com.hashmiusama.bookmanager.models;

import org.hibernate.annotations.GeneratorType;

import javax.annotation.Generated;
import javax.persistence.*;
import java.math.BigInteger;

/**
 * Created by uhashmi on 5/30/2016.
 */
@Entity
@Table(name="supervisors")
public class SupervisorModel {
    @Id
    @GeneratedValue
    int id;
    @Column(unique=true)
    String username;
    String password;
    String email;
    @Column(name="phonenumber")
    BigInteger phoneNumber;

    public SupervisorModel(String username, String password, String email, BigInteger phoneNumber) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public SupervisorModel(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername(String username) {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigInteger getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(BigInteger phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id + '\"' +
                ", \"username\":\"" + username + '\"' +
                ", \"password\":\"" + password + '\"' +
                ", \"email\":\"" + email + '\"' +
                ", \"phoneNumber\":\"" + phoneNumber + "\"}";
    }
}
