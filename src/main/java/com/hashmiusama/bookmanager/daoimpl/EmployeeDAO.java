package com.hashmiusama.bookmanager.daoimpl;

import com.hashmiusama.bookmanager.facade.HibernateSessionManager;
import com.hashmiusama.bookmanager.models.EmployeeModel;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;

/**
 * Created by uhashmi on 5/26/2016.
 */

public class EmployeeDAO implements DAO {
    Session session;
    public EmployeeDAO(){
        try {
            HibernateSessionManager.buildSessionFactory("employee");
            session = HibernateSessionManager.getSessionFactory().openSession();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public ArrayList<Object> getAll() {
        return (ArrayList<Object>) session.createCriteria(EmployeeModel.class).list();
    }

    public ArrayList<Object> getByName(String name) {
        Criteria criteria = session.createCriteria(EmployeeModel.class).add(Restrictions.eq("name",name));
        return (ArrayList<Object>) criteria.list();
    }

    public ArrayList<Object> getByPhoneNumber(String name) {
        Criteria criteria = session.createCriteria(EmployeeModel.class).add(Restrictions.eq("phoneNumber",name));
        return (ArrayList<Object>) criteria.list();
    }

    public Object getById(int id) {
        System.out.println("Getting book by id "+ id);
        EmployeeModel Employee = null;
        Employee = (EmployeeModel)session.get(EmployeeModel.class, id);
        return Employee;
    }

    public boolean insert(Object object) {
        EmployeeModel Employee = (EmployeeModel) object;
        System.out.println(Employee.toString());
        boolean returnValue = false;
        if(Integer.parseInt(session.save(Employee).toString()) > 0) {
            returnValue = true;
            session.beginTransaction().commit();
        }
        return returnValue;
    }

    public boolean update(Object object) {
        EmployeeModel Employee = (EmployeeModel) object;
        try {
            session.update(Employee);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        session.beginTransaction().commit();
        return true;
    }

    public boolean delete(Object object) {
        EmployeeModel Employee = (EmployeeModel) object;
        try {
            session.delete(Employee);
            session.beginTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void closeSession(){
        session.close();
    }
}
