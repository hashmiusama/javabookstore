package com.hashmiusama.bookmanager.daoimpl;

import com.hashmiusama.bookmanager.facade.HibernateSessionManager;
import com.hashmiusama.bookmanager.models.EmployeeModel;
import com.hashmiusama.bookmanager.models.SupervisorModel;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;

/**
 * Created by uhashmi on 5/30/2016.
 */
public class SupervisorDAO {
    Session session;
    public SupervisorDAO(){
        try {
            HibernateSessionManager.buildSessionFactory("supervisor");
            session = HibernateSessionManager.getSessionFactory().openSession();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Object> getAll() {
        return (ArrayList<Object>) session.createCriteria(SupervisorModel.class).list();
    }

    public ArrayList<Object> getByPhoneNumber(String phoneNumber) {
        Criteria criteria = session.createCriteria(SupervisorModel.class).add(Restrictions.eq("phoneNumber",phoneNumber));
        return (ArrayList<Object>) criteria.list();
    }
    public Object getByUsername(String username) {
        //System.out.println("Getting book by id "+ id);
        Criteria criteria = session.createCriteria(SupervisorModel.class).add(Restrictions.eq("username",username));
        return (Object) criteria.list().get(0);

    }


    public Object getById(int id) {
        System.out.println("Getting book by id "+ id);
        SupervisorModel supervisor = null;
        supervisor = (SupervisorModel) session.get(SupervisorModel.class, id);
        return supervisor;
    }

    public boolean insert(Object object) {
        SupervisorModel supervisor = (SupervisorModel) object;
        System.out.println(supervisor.toString());
        boolean returnValue = false;
        try{
            session.save(supervisor);
            session.beginTransaction().commit();
            returnValue = true;
        }catch(
            Exception e){e.printStackTrace();
            return false;
        }

        return returnValue;
    }

    public boolean update(Object object) {
        SupervisorModel supervisor = (SupervisorModel) object;
        try {
            session.update(supervisor);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        session.beginTransaction().commit();
        return true;
    }

    public boolean delete(Object object) {
        SupervisorModel supervisor = (SupervisorModel) object;
        try {
            session.delete(supervisor);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void closeSession(){
        session.close();
    }
}
